package test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import config.ConfigFileReader;
import helper.*;


public class TestCases {
	
	static WebDriver driver;
	
	@BeforeTest
	public void start_browser() {
		SelectBrowser sb=new SelectBrowser();
	    driver=sb.create("chrome");
		driver.manage().window().maximize();
		String URL = ConfigFileReader.getConfigValue("url");
		driver.get(URL);
		
	}
	
	@Test
	public void verify() {
		helpPOM h=new helpPOM(driver);
		h.help_dashboard("Delhi","Pune","Wed Jan 29 2020");
		h.get_flights();
		
	}
	
	@AfterTest
	public void close_browser() {
		driver.quit();
	}

}
