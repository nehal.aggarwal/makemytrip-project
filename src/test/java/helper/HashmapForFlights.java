package helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

public class HashmapForFlights {
	
	static Map<String, ArrayList<Integer>> map=new HashMap<String, ArrayList<Integer>>();
	
	public void set_map(List<WebElement> flights,List<WebElement> price) {
		
	    for(int i=0;i<flights.size();i++) {
	    	ArrayList<Integer> a=new ArrayList<Integer>();
	    	if(map.containsKey(flights.get(i).getText())) {
	    		a=map.get(flights.get(i).getText());
	    		int value=StringToInteger(price.get(i).getText());
	    		a.add(value);
	    	}
	    	
	    	map.put(flights.get(i).getText(), a);
	    }
	}
	
	public void print_map_withMinMax() {
		
	    for (Map.Entry<String,ArrayList<Integer>> entry : map.entrySet()) {  
            ArrayList<Integer> a=entry.getValue();
            int min=Collections.min(a);
            int max=Collections.max(a);
            System.out.println("Flight: "+entry.getKey()+" | No.of flights: "+a.size()+" | Min price: "+min+" | Max price: "+max);
        } 
	}
	
	private int StringToInteger(String text) {
		String[] s = text.split(" ");
		String price = s[1];
		int ans = 0;
		for (int i = 0; i < price.length(); i++) {
			if (price.charAt(i) != ',')
				ans = ans * 10 + price.charAt(i) - '0';
		}
		return ans;
	}

}
