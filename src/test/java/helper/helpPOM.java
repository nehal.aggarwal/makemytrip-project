package helper;

import org.openqa.selenium.WebDriver;

import page0bjects.Dashboard;
import page0bjects.Flights;

public class helpPOM {
	
	WebDriver driver;
	
	public helpPOM(WebDriver driver) {
		this.driver=driver;
	}
	
	public void help_dashboard(String from,String to,String date) {
		Dashboard db=new Dashboard(driver);
		db.select_type();
		db.select_from(from);
		db.select_to(to);
		db.select_departure(date);
		db.select_search();
	}
	
	public void get_flights() {
		Flights f=new Flights(driver);
		f.select_airlines();
	}

}
