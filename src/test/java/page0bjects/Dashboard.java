package page0bjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Dashboard {
	
WebDriver driver;
	
	public Dashboard(WebDriver driver) {
		this.driver=driver;
	}

	public void select_type() {
		driver.findElement(By.xpath(".//ul[@class='makeFlex font12']//li[1]")).click();
		driver.findElement(By.xpath(".//ul[@class='fswTabs latoBlack greyText']//li[1]")).click();
		
	}

	public void select_from(String from) {
		driver.findElement(By.xpath(".//div[@class='fsw_inputBox searchCity inactiveWidget ']")).click();
		driver.findElement(By.xpath(".//input[@placeholder='From']")).sendKeys(from);
		try {
			Thread.sleep(1000);
		}
		catch(Exception e) {
			System.out.println("Exception");
		}
		String path=".//ul[@role='listbox']/li/div/div/p[contains(.,'"+from+"')]";
		driver.findElement(By.xpath(path)).click();
	}

	public void select_to(String to) {
		
		driver.findElement(By.xpath(".//input[@placeholder='To']")).sendKeys(to);
		try {
			Thread.sleep(1000);
		}
		catch(Exception e) {
			System.out.println("Exception");
		}
		String path=".//ul[@role='listbox']/li/div/div/p[contains(.,'"+to+"')]";
		driver.findElement(By.xpath(path)).click();
	}

	public void select_departure(String date) {
		
		try {
			Thread.sleep(1000);
		}
		catch(Exception e) {
			System.out.println("Exception");
		}
		String path=".//div[@aria-label='"+date+"']";
		driver.findElement(By.xpath(path)).click();
	}
	
	public void select_search() {
		driver.findElement(By.xpath(".//a[@class='primaryBtn font24 latoBlack widgetSearchBtn ']")).click();
		
	}
	
	

}
