package page0bjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import helper.HashmapForFlights;

public class Flights {

	WebDriver driver;

	public Flights(WebDriver driver) {
		this.driver = driver;
	}

	public void select_airlines() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			long lastHeight = (Long) js.executeScript("return document.body.scrollHeight");

			while (true) {
				js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
				Thread.sleep(2000);

				long newHeight = (Long) js.executeScript("return document.body.scrollHeight");
				if (newHeight == lastHeight) {
					break;
				}
				lastHeight = newHeight;
			}
			List<WebElement> flights = driver.findElements(By.xpath(".//span[@class='airways-name ']"));
			List<WebElement> price = driver.findElements(By.xpath(".//span[@class='actual-price']"));
			int size = flights.size();
			System.out.println("Total no. of flights available: " + size);
			
			HashmapForFlights hs=new HashmapForFlights();
			hs.set_map(flights,price);
			
			HashmapForFlights h1=new HashmapForFlights();
			h1.print_map_withMinMax();

		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
